# Arch Linux Backup and Restore Tool

A simple shell script to backup and restore your AL installation.

See `archlinux-backup --help` for full usage.
